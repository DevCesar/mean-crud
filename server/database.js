const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://localhost:27017/mean-crud',(err, res)=>{
    
     if(err){
         throw err;
     }else{
         console.log('Conexion Establecida...')
         
     }
});
  

module.exports = mongoose;